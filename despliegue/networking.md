# Consideraciones a tomar en cuanto a las red (VPC) en el despliegue de instancias utilizado sub-redes privadas

## Escenario de Despliegue Actual:


### VPC utilizada
`
vpcjobpointsv : vpc-03dd13c47df9a7618
`
### Dos (02) subredes publicas (IGW)
```
ub4jobpointsv-public : subnet-0c7ecc9bb0185902d

sub5jobpointsv-public : subnet-0aea13e60bb36d444

```

### Dos (02) subredes Privadas (NGW)
```
sub5jobpointsv-private : subnet-02d4e4ccd316b086c

sub6jobpointsv-private : subnet-05bd2c4a30a1621ce

```
### Security Group:
```
work-sg-fargate-web-db-nfs : sg-028a7d3d280e03ab8
```
### Host Bastión:

Necesario para administrar los puntos de montaje de las aplicaciones en el EFS y para acceso administrativo a la BD.


### Importante :
```
1.- Cuando se realiza el despliegue de instancias en AWS FARGATE utilizando sub-redes privadas, éstas deben tener
configurado un NAT GATEWAY ( obligatorio ) para que puedan alcanzar a los endpoint de los diferentes servicios
de AWS, asi como en los casos cuando se utilizan repositorios públicos.

2.- En el caso de configurar ALB para las instancias FARGATE desplegadas en sub-redes privadas, el balanceador
ALB debe desplegarse en sub-red pública.
```




