# En el EFS: (Elastic Filesystem)

1.- crear los access point.

2.- Crear el arbol dentro del filesystem de la aplicacion a desplegar (/mnt/efs/fs1)

```
yum install httpd (permisos apache)
```

## Ejemplo para btmov:

Permisos necesarios para la correcta ejecucion de la aplicacion:

```
chmod -R 755 /mnt/efs/fs1/btmov/var/www/html

chown -R www-data.www-data /mnt/efs/fs1/btmov/var/www/html

```

## Para instalar cliente mysql en el bastión:

```
yum install mariadb105-server-utils-10.5.18-1.amzn2023.0.1.x86_64

mysql -h global-db-ecommerce.crvfokzi5rw6.us-east-1.rds.amazonaws.com -u admin -p

contraseña: XXXXXXXXXXXXXX
```
## Copiar el codigo fuente a los puntos de montaje:
```
scp -rv -i /home/ec2-user/archivo.pem ec2-user@ec2-54-209-208-10.compute-1.amazonaws.com:/mnt/efs/fs1/btmov .
```
## Montar el filesystem en la maquina host Bastion (Amazon Linux)

```
mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport fs-08810f315d
b7825ce.efs.us-east-1.amazonaws.com:/ /mnt/efs/fs1
```




