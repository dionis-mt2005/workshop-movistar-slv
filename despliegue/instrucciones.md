# Instrucciones de Despliegue:

## VPC / EC2

## Paso 1:
```
Creación de VPC, se recomienda dos (02) sub-redes públicas y dos (02) sub-redes privadas.

Despliegue Host Bastion EC2 Amazon Linux (Administrar el RDS y realizar los montajes/revisión  de los EFS)

Cree Security Groups para el acceso entre las instancias de Fargate, Load Balancers, RDS, Bastión y EFS.

( Puertos 22, 80, 443, 2049, 3389 ).

```
## Paso 2: AWS ECR

```
Preparación de la imagen de Docker
Creación de un Dockerfile
Construcción de la imagen de Docker
Repositorio ECR -> Creación de un repositorio ECR
Publicación de la imagen de Docker en ECR
```
## Paso 3 : AWS EFS
```
Crear filesystem EFS.

Crear los puntos de montaje.
```
## Paso 4: AWS RDS
```
Crear Base de datos RDS Mysql version 5.7 .
```
## Paso 4: AWS Fargate
```
Creación de Cluster Fargate
. Task definition (referenciado hacia los volumen EFS)
. Service.
. Despliegue en balanceador de Carga .
```


