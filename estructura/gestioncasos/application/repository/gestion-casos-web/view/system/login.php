<main class="form-signin">
    <center><img class="mb-4" src="resource/image/logom-@x2.png" alt="">
        <h1 class="h3 mb-3 fw-normal">Bienvenido</h1>
    </center>
    <form name="frmIniciarSesion" id="frmIniciarSesion" class="d-block" onsubmit="return false" autocomplete="off">
        <div class="form-floating mb-3">
            <input type="text" name="username" id="usuario" class="form-control" autofocus="true" >
            <label for="username">Usuario</label>
        </div>
        <div class="form-floating mb-3">
            <input type="password" name="password" id="password" class="form-control" >
            <label for="password">Contraseña</label>
        </div>
        <button type="submit" class="w-100 btn btn-lg btn-primary" id="btnIniciarSesion" form="frmIniciarSesion"><i class="fa fa-sign-in" aria-hidden="true"></i>Iniciar Sesi&oacuten</button>
    </form>
    <p class="mt-5 mb-3 text-muted">Movistar© 2023</p>
</main>
<script type="text/javascript" src="controller/system/login.js"></script>