
<div class="container-fluid">
  <div class="row justify-content-center my-3 mx-2">
    <h2>Informaci&oacute;n del Caso</h2>   
    <form id="frmInfo" name="frmInfo" class="d-block" onsubmit="return false" autocomplete="off">
    <div class="row">
      <input type="hidden" id="createdBy" class="form-control" name="createdBy" value= <?php echo($_SESSION[__SESSION_NAME__]['username']) ?> readonly="">
    </div>
    <div class="card shadow p-0 my-2">

      <div class="card-body">
            <div class="row d-none" id="cerrar-caso">
              <div class="col-12 d-flex justify-content-end mb-2">
              <button class="btn btn-primary" form="frmInfo" id="btnCerrar">
                Cerrar Caso
              </button>
              </div>
            </div>   
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group mb-3">
                  <label for="id">Id Caso</label> 
                  <input type="text" id="id" class="form-control" name="id" placeholder="Numero de caso" 
                  value="<?php echo(((isset($_GET['id'])) ? $_GET['id']:'')); ?>" readonly="">
                </div>
              </div>
              <div class="col-sm-6">
                  <div class="form-group mb-3">
                    <label for="nombreCliente">Cliente</label> 
                    <input type="text" id="nombreCliente" class="form-control" name="nombreCliente" placeholder="Nombre del cliente" readonly="">
                  </div>
                </div>              
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group mb-3">
                  <label for="numeroOrigen">N&uacute;mero Origen</label> 
                  <input type="text" id="numeroOrigen" name="numeroOrigen" class="form-control" readonly="">                                
                 </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group mb-3">
                  <label for="swin">Instancia</label> 
                  <input type="text" id="swin" class="form-control" name="swin" placeholder="Instancia" readonly="">
                </div>
              </div>  
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group mb-3">
                        <label for="status">Estado</label> 
                        <input type="text" id="status" class="form-control" name="status" placeholder="Estado" readonly="">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group mb-3">
                        <label for="priority">Prioridad</label> 
                        <input type="text" id="priority" class="form-control" name="priority" placeholder="Prioridad" readonly="">
                      </div>
                    </div>  
                  </div>
              </div>
              <div class="col-md-6">
                <div class="form-group mb-3">
                  <label for="fixTime">Area Origen</label> 
                  <input type="text" id="fixTime" class="form-control" name="fixTime" readonly="">
                </div>
              </div>  
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group mb-3">
                  <label for="nombreContacto">Nombre Contacto</label> 
                  <input type="text" id="nombreContacto" class="form-control" name="nombreContacto" readonly="">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group mb-3">
                  <label for="numeroContacto">N&uacute;mero Contacto</label> 
                  <input type="text" id="numeroContacto" class="form-control" name="numeroContacto" placeholder="N&uacute;mero Contacto" readonly="">
                </div>
              </div>  
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group mb-3">
                  <label for="lineaNegocio">L&iacute;nea de Negocio</label> 
                  <input type="text" id="lineaNegocio" name="lineaNegocio" class="form-control" readonly="">                                
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group mb-3">
                    <label for="tipologia1">L&iacute;nea de Gesti&oacute;n</label> 
                    <input type="text" id="tipologia1" class="form-control" name="tipologia1"  readonly="">  </div>
              </div>   
            </div>   
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group mb-3">
                <label for="tipologia1">Tipolog&iacute;a</label> 
                    <input type="text" id="tipologia2" name="tipologia2" class="form-control" readonly="">                                
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group mb-3">
                    <label for="tipologia2">Sub Tipolog&iacute;a</label> 
                    <input type="text" id="tipologia3" name="tipologia3" class="form-control" readonly="">                                
                </div>
              </div>   
            </div>   
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="note"> Nota </label>
                  <textarea class="form-control" id="note" name="note" rows="4" readonly=""></textarea>
                </div>
              </div>        
            </div>    
            <div class="row mt-3 mb-3">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="note"> Resoluci&oacute;n </label>
                  <textarea class="form-control" id="resolucion" name="resolucion" rows="4" readonly=""></textarea>
                </div>
              </div>        
            </div> 
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript" src="controller/casos/detalle.js"></script>