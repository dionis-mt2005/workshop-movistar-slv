
<div class="container-fluid">
    <div class="row justify-content-center my-3 mx-2">
        <h2>Nuevo Caso</h2>
        <div class="card shadow p-0 my-2">
            <div class="card-body m-2">
                <form name="frmCrearCaso" id="frmCrearCaso" class="d-block" onsubmit="return false;" autocomplete="off">
                    <div class="row">
                        <input type="hidden" id="createdBy" class="form-control" name="createdBy" value= <?php echo($_SESSION[__SESSION_NAME__]['username']) ?> readonly="">
                        <input type="hidden" id="priority" class="form-control" name="priority" placeholder="Prioridad" value="2. Media" readonly="">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-sm-6" id="col-numOrigen">
                                    <div class="form-group mb-3">
                                        <label for="numeroOrigen">N&uacute;mero Origen</label> 
                                        <input type="text" id="numeroOrigen" class="form-control" name="numeroOrigen" placeholder="Ingrese telefono">
                                        <span class="help-block"><small>Ingrese n&uacute;mero de t&eacute;lefono.</small></span>
                                     </div>
                                </div>
                                <div class="col-sm-6" id="col-swin">
                                    <div class="form-group mb-3">
                                    <label for="swin">Instancia</label> 
                                    <input type="text" id="swin" class="form-control" name="swin" placeholder="Instancia">
                                    <span class="help-block"><small>Ingrese instancia</small></span>
                                </div>
                             </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mb-3">
                                <label for="nombreContacto">Nombre Contacto</label> 
                                <input type="text" id="nombreContacto" class="form-control" name="nombreContacto" placeholder="Nombre Contacto">
                            </div>
                        </div> 
                        
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group mb-3">
                                <label for="numeroContacto">N&uacute;mero Contacto</label> 
                                <input type="text" id="numeroContacto" class="form-control" name="numeroContacto" placeholder="N&uacute;mero Contacto">
                            </div>
                        </div> 
                        <div class="col-sm-6">
                            <div class="form-group mb-3">
                            <label for="fixTime">Area Origen</label>
                                <select class="form-control capitalize" id="fixTime" name="fixTime" aria-invalid="false">
                                    <option value="ATENCION ESPECIALIZADA" >ATENCION ESPECIALIZADA</option>
                                    <option value="ATENCION PYMES" >ATENCION PYMES</option>
                                </select>    
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="form-group mb-3">
                                <label for="lineaNegocio">L&iacute;nea de negocio</label>
                                <input type="text" id="lineaNegocio" class="form-control" name="lineaNegocio" placeholder="Linea de negocio" readonly=""> 
                            </div>
                        </div>  
                        <div class="col-md-6">
                            <div class="form-group mb-3">
                            <label for="tipologia1">L&iacute;nea de Gesti&oacute;n</label>
                                <select class="form-control" id="tipologia1" name="tipologia1" aria-invalid="false"></select>      
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-3">
                                <label for="tipologia2">Tipolog&iacute;a </label>
                                <select class="form-control" id="tipologia2" name="tipologia2" aria-invalid="false"></select>    
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mb-3">
                                <label for="tipologia3">Sub Tipolog&iacute;a </label>
                                <select class="form-control" id="tipologia3" name="tipologia3" aria-invalid="false"></select>       
                            </div>
                        </div>       
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="note"> Nota </label>
                                <textarea class="form-control" id="note" name="note" rows="4" placeholder="Ingrese nota"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 d-flex justify-content-end pt-4">
                            <button type="submit" class="btn btn-primary" id="btnCrear" form="frmCrearCaso">
                                <i class="fa fa-bookmark-o" aria-hidden="true"></i> Guardar
                            </button>  
                        </div>
                    </div>
                </form>
            </div>
        </div>
  </div>
</div>

<script type="text/javascript" src="controller/casos/crear.js"></script>