
<div class="container-fluid">
    <div class="row justify-content-center my-3 mx-2">
        <div class="col-12">
            <h2 class=>Busqueda de Casos por T&eacute;lefono o Instancia</h2>
            <div class="card shadow p-0 my-2">
                <div class="card-body">
                    <form name="frmBuscarXTelefono" id="frmBuscarXTelefono" class="d-block" onsubmit="return false" autocomplete="off">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group mb-3">
                                            <input type="text" id="numeroContacto" class="form-control" name="numeroContacto" placeholder="Ingrese el Tel&eacute;fono o Instancia" autofocus >
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <p class="mb-2"><strong>Busqueda por:</strong></p>
                                        <div class="form-check form-check-inline">
                                            <input type="radio" id="optionBuscar" name="optionBuscar" class="form-check-input" value="1" checked="">
                                            <label class="form-check-label" for="customRadio2">Tel&eacute;fono:</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input type="radio" id="optionBuscar2" name="optionBuscar" class="form-check-input" value="2">
                                            <label class="form-check-label" for="customRadio2">Instancia</label>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>           
                            <div class="col-sm-6">
                                <button class="btn btn-primary" form="frmBuscarXTelefono" id="btnBuscar">
                                    <i class="fa fa-bookmark-o" aria-hidden="true"></i> Buscar
                                </button>  
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="card shadow p-0 my-2 d-none" id="info-vacio">
                <div class="card-body">
                    <center>
                        <h6>No se encontraron casos registrados</h6>
                    </center>
                </div>
            </div>
            <div class="card shadow p-0 my-2 d-none" id="info">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="tblCasos" class="table datatables" style="width: 100%">
                            <thead>
                                <tr>
                                    <th >Id</th>
                                    <th>Sub Tipolog&iacute;a</th>
                                    <th>N&uacute;mero Origen/Instancia</th>
                                    <th>Estado</th>
                                </tr>
                            </thead>
                            <tbody id="tblBodyCasos">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="controller/casos/buscar-telefono.js"></script>