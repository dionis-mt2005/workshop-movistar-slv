<?php

require_once(__BASE_URI_SYS__."vendor/autoload.php");

  class rest {

    public function __construct(){
    }

    public function callApi($url='', $method='GET', $params=array()){
      $data=array();
      $response=array();
      $token=((isset($_SESSION[__SESSION_NAME__]['token'])) ? $_SESSION[__SESSION_NAME__]['token']:'');
      
      try {
          switch($method) {
            case 'GET':
              $request=new HTTP_Request2($url, HTTP_Request2::METHOD_GET);
              $request->setHeader('Authorization', 'Bearer ' . $token);
              if (!empty($params)) {
                $request->getUrl()->setQueryVariables($params);
              }
            break;
            case 'POST':
              $request=new HTTP_Request2($url, HTTP_Request2::METHOD_POST);
              $request->setHeader('Authorization', 'Bearer ' . $token);
              $request->setHeader('Content-type: application/json');
          
              $request->setBody(json_encode($params));
            break;
            case 'PUT':
              $request=new HTTP_Request2($url, HTTP_Request2::METHOD_PUT);
              $request->setHeader('Authorization', 'Bearer ' . $token);
              $request->setHeader('Content-type: application/json');
              if (!empty($params)) {
                $request->getUrl()->setQueryVariables($params);
              }
            break;
          }
          $request->setAdapter('curl');
          $request->setHeader(array('Connection' => 'close', 'User-Agent' => $_SERVER['HTTP_USER_AGENT']));
          //print_r($request);
          $result=$request->send();
          //print_r($result);       
          switch($result->getStatus()){
            case '200':
              $data=json_decode($result->getBody(), true);
              $response=array('status'=>true, 'data'=>$data, 'mensaje'=>'Operación exitosa', 'codigo'=>200);
            break;
            case '400':
              $response=array('status'=>false, 'data'=>$data, 'mensaje'=>'Falta informaci&oacuten', 'codigo'=>400);
            break;
            case '401':
              $data=json_decode($result->getBody(), true);
              $response=array('status'=>false, 'data'=>$data, 'mensaje'=>'Credenciales incorrectas', 'codigo'=>401);
            break;
            case '403':              
              if(isset($_SESSION[__SESSION_NAME__])){ 
                  unset($_SESSION[__SESSION_NAME__]);                 
                  $response=array('status'=>false, 'data'=>$data, 'mensaje'=>'Se expiro su sesion', 'codigo'=>403);
              }else{
                $data=json_decode($result->getBody(), true);
                $response=array('status'=>false, 'data'=>$data, 'mensaje'=>'Contraseña Incorrecta', 'codigo'=>403);
              }
            break;
            case '404':
                $response=array('status'=>false, 'data'=>$data, 'mensaje'=>'No se encontro ningun valor', 'codigo'=>404);
            break;
            default:
            $data=json_decode($result->getBody(), true);
            $response=array('status'=>false, 'data'=>$data, 'mensaje'=>'Error', 'codigo'=>400);
            break;
          }
      } catch (HTTP_Request2_Exception $e) {
          $response=array('status'=>false, 'mensaje'=>((__MODE__=='dev') ? $e->getMessage():'No fue posible devolver ningun resultado'));
      }
      return $response;
    }
  }
?>