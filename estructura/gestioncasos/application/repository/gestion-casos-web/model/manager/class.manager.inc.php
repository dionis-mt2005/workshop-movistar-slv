<?php
/* Instancia la clase y el metodo a utilizar */
	class manager {

		public function load($model='') {
			$result=array();
			if ($model!='') {
				$class=substr($model, 0, strpos($model, '_'));
				$method=substr($model, strpos($model, '_')+1);
				$file='class/class.'.$class.'.inc.php';
				if (file_exists($file)) {
					require_once($file);
					$object=new $class();
					if (method_exists($object, $method)) {
						//if(isset($_SESSION[__SESSION_NAME__]) || in_array($method, array('login', 'logout'))){
							$result=$object->$method();
						//} else {
							//$result=array('status'=>false, 'mensaje'=>'La sesión ha expirado');	
						//}
					} else{
						$result=array('status'=>false, 'mensaje'=>'Metodo no encontrado');	
					}
				} else {
					$result=array('status'=>false, 'mensaje'=>'Clase no encontrada');
				}
			} else {
				$result=array('status'=>false, 'mensaje'=>'El valor enviado es incorrecto');
			}
			return $result;
		}
	}

?>
