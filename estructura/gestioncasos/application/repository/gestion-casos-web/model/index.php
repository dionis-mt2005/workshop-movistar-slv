<?php
	include("../config/settings.inc.php");
	require_once(__BASE_URI_SYS__."model/manager/class.manager.inc.php");
	
	$model='';
	if (isset($_POST['op']) || isset($_GET['op']) || isset($_REQUEST['op'])) {
		if (isset($_POST['op']) && $_POST['op']!='') {
			$model=$_POST['op'];
		} else {
			if ($_GET['op']!='') {
				$model=$_GET['op'];
			} else {
				$model=$_REQUEST['op'];
			}
		}
	} 

	$manager=new manager();
	echo json_encode($manager->load($model));
?>