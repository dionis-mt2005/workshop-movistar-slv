<?php
    require_once("manager/class.rest.inc.php");

    class usuario extends rest {

        function __construct() {

        }

        public function login() {
            $response=array();
            $params=$_POST['form'];

            $url = __BASE_URI_HTTP_API__.'auth/authenticate';
            $param_list = array('username'=>$params['username'], 'password'=>$params['password']);
            $response=$this->callApi($url, 'POST', $param_list);
            if ($response['data']['codigo']==200){
                $_SESSION[__SESSION_NAME__]['token']=$response['data']['token'];
                $_SESSION[__SESSION_NAME__]['username']=$response['data']['username'];
            } 
            return $response;
        }
        
        public function logout(){
            $response=array();
            unset($_SESSION[__SESSION_NAME__]);
            session_destroy();
            $response=array("status"=>true);
            return $response;
        }
    }
?>