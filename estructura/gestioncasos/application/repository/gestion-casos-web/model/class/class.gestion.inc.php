<?php
    require_once("manager/class.rest.inc.php");

    class gestion extends rest {

        function __construct() {
        }
        public function buscarCasoXId(){
            $result=array();
            $response=array();
            $params=$_GET['form'];
            $url = __BASE_URI_HTTP_API__.'case/'.$params['id'];
            $response=$this->callApi($url, 'GET', array()); 
            if($response['status']) {
                $result=$this->listarResolucion($params['id']);
                if($result['status']){
                    if(count($result['data'])>0){
                        $response['data']['observaciones']=$result['data'][(count($result['data'])-1)]['observaciones'];
                        $response['data']['fechaCreacion']=$result['data'][(count($result['data'])-1)]['creationDate'];
                    } else {
                        $response['data']['observaciones']='No hay observaciones';
                    }
                }
            }
            return $response;
        }

        public function guardar(){
            $response=array();
            $params=$_POST['form'];
           
            $params['swin']=(($params['swin']!='') ? $params['swin']:null); 
            $params['numeroOrigen']=(($params['numeroOrigen']!='') ? $params['numeroOrigen']:null); 
            $params['note']=(($params['note']!='') ? $params['note']:null); 
           //print_r($params);
            $url = __BASE_URI_HTTP_API__.'case/add';
            $response=$this->callApi($url, 'POST', $params);
            return $response;
        }

        public function buscarXTelefono(){
            $response=array();
            $params=$_GET['form'];  
            $url = __BASE_URI_HTTP_API__.'case/list/'.$params['numeroContacto'].'/'.$params['optionBuscar'];
            $response=$this->callApi($url, 'GET', array());  
            return $response;
        }

        public function listarResolucion($id){
            $response=array();
            $url = __BASE_URI_HTTP_API__.'gestion/list/'.$id;
            $response=$this->callApi($url, 'GET', array()); 
            return $response;
        }
        
        public function lineaNegocio(){
            $response=array();
            $params=$_GET;
            $url = __BASE_URI_HTTP_API__.'tipologia/instancia/'.$params['parametro'];
            //print_r($url);
            $response=$this->callApi($url, 'GET', array());  
            return $response;
        }
        public function obtenerTipologia1(){
            $response=array();
            $params=$_POST['form'];
            $url = __BASE_URI_HTTP_API__.'tipologia/tpg1';
            $response=$this->callApi($url, 'POST', $params);
            //print_r($response);
            return $response;
        }
        public function obtenerTipologia2(){
            $response=array();
            $params=$_POST['form'];
            $url = __BASE_URI_HTTP_API__.'tipologia/tpg2';
            $response=$this->callApi($url, 'POST', $params);
            //print_r($response);
            return $response;
        }
        public function obtenerTipologia3(){
            $response=array();
            $params=$_POST['form'];
            $url = __BASE_URI_HTTP_API__.'tipologia/tpg3';
            $response=$this->callApi($url, 'POST', $params);
            //print_r($response);
            return $response;
        }

        public function cerrarCaso(){
            $response=array();
            $params=$_GET;
            $url = __BASE_URI_HTTP_API__.'case/status/'.$params['id'].'/'.$params['createdBy'];
            $response=$this->callApi($url, 'PUT', array());  
            return $response;
        }
    }
?>