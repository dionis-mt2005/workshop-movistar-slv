<?php
    require_once('config/config.inc.php');
  
    $view = '';
    $page_path = '';
    //session_destroy();
    // Evaluar si se envia un valor por metodo GET
    if (isset($_GET['page'])) {
        $view = $_GET['page'];
    } else {
        $view = __VIEW_LOGIN__;
    }
    
    if (isset($page[$view]['template'])) {
        $page_path = __TEMPLATE_PATH__ . '/' . $page[$view]['template'];
        if (!empty($page[$view]['template'])) {
            include($page_path);
        }
    } else {
        $view = __VIEW_ERROR__; 
        $page_path = __TEMPLATE_PATH__ . '/' . $page[$view]['template'];
        include($page_path);
    }

    

?>