<!DOCTYPE html>
<html lang="es" xml:lang="es">
    <head>
        <title><?php echo __SYSTEM_NAME__; ?></title>
        <base href="<?php echo __BASE_URI_HTTP__; ?>" />
        <link rel="icon" type="image/x-icon" href="resource/image/logom-@x2.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta http-equiv="Expires" content="0" />
        <meta http-equiv="Last-Modified" content="0" />
        <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <!-- Fonts CSS -->
        <link href="resource/fontawesome/css/all.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="resource/DataTables/datatables.min.css"/>
        <script scr="resource/fontawesome/js/all.js"></script>
        <link href="https://fonts.googleapis.com/css2?family=Overpass:ital,wght@0,100;0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        <script src="resource/jquery/jquery-3.6.0.min.js"></script>
        <script type="text/javascript" src="resource/DataTables/datatables.min.js"></script>
        <script src="resource/bootstrap/js/dataTables.bootstrap5.min.js"></script>
        <script type="text/javascript" src="resource/dist/js/settings.js"></script>
        <script type="text/javascript" src="controller/system/logout.js"></script>
        <!--Jquery Validation-->
        <script type="text/javascript" src="resource/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="resource/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="resource/jquery-validation/localization/messages_es.js"></script>
        <script type="text/javascript" src="resource/dist/js/validate.js"></script>
         <!-- Select2 -->         
         <link href="resource/select2/select2.css" rel="stylesheet" />
         <link href="resource/select2/select2-bootstrap4.css" rel="stylesheet" />
        <script type="text/javascript" src="resource/select2/select2.min.js"></script>
        <!-- Sweet Alert -->
        <script src="resource/sweetalert2/sweetalert2.js"></script>
        <link rel="stylesheet" type="text/css" href="resource/sweetalert2/sweetalert2.css">
        <!--sidebar -->
        <link href="resource/dist/css/style.css" rel="stylesheet" />
        <link href="resource/sidebar/sidebar.css" rel="stylesheet" />
    </head>
    <body>
        <div class="d-flex" id="wrapper">
            <!-- Sidebar-->
            <div class="border-end bg-white" id="sidebar-wrapper">
                <div class="sidebar-heading text-center"><img class="mb-4" src="resource/image/logom-@x2.png" alt="" width="60px"></div>
                <div class="list-group list-group-flush">
                    <a class="list-group-item p-3" href="crear">
                        <span class="mx-2"><i class="fa fa-plus" aria-hidden="true"></i> </span> Nuevo
                    </a>
                    <a class="list-group-item list-group-item-light p-3" href="buscar">
                        <span class="mx-2"><i class="fa fa-search" aria-hidden="true"></i></span> Buscar
                    </a>
                    <a class="list-group-item list-group-item-light p-3" href="buscar-telefono">
                        <span class="mx-2"><i class="fa fa-search" aria-hidden="true"></i></span> Busqueda por T&eacute;lefono
                    </a>
                </div>
            </div>
            <!-- Page content wrapper-->
            <div id="page-content-wrapper">
                <!-- Top navigation-->
                <nav class="navbar navbar-expand-lg navbar-light ">
                    <div class="container-fluid">
                        <button class="btn" id="sidebarToggle"><span class="navbar-toggler-icon"></span></button>
                        <div class="d-flex justify-content-between">
                        <span class="me-2 navbar-text" id="usuario">
                        <?php echo($_SESSION[__SESSION_NAME__]['username']) ?>
                       
                        </span> 
                            <button id="salir" class="btn btn-primary" type="button"> Salir </button>
                    </div>
                    </div>
                </nav>
                <!-- Page content-->
                <div class="container-fluid">
                <span></span>
                <?php include(__VIEW_PATH__ . "/" . $page[$view]['file']); ?>
                </div>
            </div>
        </div>
    </body>
    <style>
    
    
    </style>

    <script type="text/javascript" src="resource/sidebar/sidebar.js"></script>
</html>
