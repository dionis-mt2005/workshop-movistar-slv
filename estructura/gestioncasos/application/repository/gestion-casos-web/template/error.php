<!DOCTYPE html>
<html lang="es" xml:lang="es">
  <head>
    <title><?php echo __SYSTEM_NAME__; ?></title>
    <base href="<?php echo __BASE_URI_HTTP__; ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  </head>
  <body>
      <h1>ERROR</h1>
      <?php include(__VIEW_PATH__ . "/" . $page[$view]['file']); ?>
  </body>
</html>