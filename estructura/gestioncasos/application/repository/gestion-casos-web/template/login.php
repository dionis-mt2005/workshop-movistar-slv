<!DOCTYPE html>
<html lang="es" xml:lang="es">
  <head>
    <title><?php echo __SYSTEM_NAME__; ?></title>
    <base href="<?php echo __BASE_URI_HTTP__; ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
 
    <script src="resource/jquery/jquery-3.6.0.min.js"></script>
    <!-- Bootstrap Bundle with Popper -->
    <script src="resource/bootstrap/js/bootstrap.bundle.min.js" ></script>
    <!--Icons FontAwesome-->
    <script scr="resource/fontawesome/js/all.js"></script>
    <script type="text/javascript" src="resource/dist/js/settings.js"></script>
    <!--Jquery Validation-->
    <script type="text/javascript" src="resource/jquery-validation/jquery.validate.js"></script>
    <script type="text/javascript" src="resource/jquery-validation/additional-methods.js"></script>
    <script type="text/javascript" src="resource/jquery-validation/localization/messages_es.js"></script>
   <!-- Sweet Alert -->
    <script src="resource/sweetalert2/sweetalert2.js"></script>
    <link rel="stylesheet" type="text/css" href="resource/sweetalert2/sweetalert2.css">
    <link href="resource/dist/css/signin.css" rel="stylesheet">

  </head>
  <body>
      <?php include(__VIEW_PATH__ . "/" . $page[$view]['file']); ?>
  </body>
</html>