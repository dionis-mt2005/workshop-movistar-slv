$(document).ready(function() {
  $('#salir').click(function(){
      Swal.fire({
        type: 'warning',
        title: 'Información',
        html: 'Esta seguro que desea cerrar sesión',
        showCancelButton: true,
        confirmButtonText: 'Aceptar'
      }).then((result) => {
        if (result.value) {
          logout(1);
        }
      });
  });
});

function logout(tipoLogout){
  $.ajax({
    url: 'model/',
    type: 'POST',
    dataType: 'json',
    data: {
        op: 'usuario_logout',
    },
    success: function(response) {
      if(response.status==true){

        if(tipoLogout==1){
  /*        Swal.fire({
            type: 'warning',
            title: 'Información',
            html: 'La sesión se finalizo con exito',
            showCancelButton: false,
            confirmButtonText: 'Aceptar'
          }).then((result) => {*/
            location.href='login';
          /*});*/
        } else {
          Swal.fire({
            type: 'warning',
            title: 'Información',
            html: 'Su sesión ha expirado',
            showCancelButton: false,
            confirmButtonText: 'Aceptar'
          }).then((result) => {
            location.href='login';
          });
        }
      } else {
        Swal.fire({
          type: 'danger',
          title: 'Alerta',
          html: response.mensaje,
          showCancelButton: false,
          confirmButtonText: 'Aceptar'
        }).then((result) => {
          location.href='login';
        });
      }
    },
    error: function() {
      Swal.fire({
        type: 'danger',
        title: 'Alerta',
        html: response.mensaje,
        showCancelButton: false,
        confirmButtonText: 'Aceptar'
      }).then((result) => {
        location.href='login';
      });
    }
  });
}