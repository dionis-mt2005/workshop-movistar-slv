$(document).ready(function() {

  $('#btnIniciarSesion').click(function() {
    $('#frmIniciarSesion').validate({
      ignore: "",
      rules: {
        username: {
            required: true            
        },
        password: {
          required: true 
        }
        
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
      },
      highlight: function(element, errorClass, validClass) {
          $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
      },
      submitHandler: function(form) {
        $.ajax({
          url: 'model/',
          type: 'POST',
          dataType: 'json',
          data: {
            op: 'usuario_login',
            form: $('#frmIniciarSesion').formJson()
          },
         
          success: function(response) {
            if(response.status){
                location.href='crear';
            }else{
              Swal.fire({
                type: 'warning',
                title: 'Atenci&oacute;n.',
                html: response.mensaje,
                showCancelButton: false,
                confirmButtonText: 'Aceptar'
              });
            }

          },        
          error: function() {
            Swal.fire({
              type: 'error',
              title: 'Error.',
              html: 'Error al realizar la petici&oacute;n',
              showCancelButton: false,
              confirmButtonText: 'Aceptar'
            });
          }
        });
      }
    });
  });
  
});
