$(document).ready(function() {  
    $('#btnCerrar').click(function() {
        Swal.fire({
            type: 'warning',
            title: '<h5>Advertencia: </h5> ',
            html: 'Desea cerrar el caso: '+$('#id').val(),
            showCancelButton: true,
            confirmButtonText: 'Aceptar'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: 'model/',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        op: 'gestion_cerrarCaso',
                        id: $('#id').val(),
                        createdBy: $('#createdBy').val()

                    },
                    success: function(response) {
                        if(response.status){
                            Swal.fire({
                                type: 'info',
                                title: '<h5>Caso: '+response.data.id+' </h5> ',
                                html: ' Actualizado correctamente: ',
                                showCancelButton: false,
                                confirmButtonText: 'Aceptar'
                            }).then((result) => {
                                location.href='detalle/'+response.data.id;
                            });
                        }else{
                            mensajeResponse(response,response.data.id)
                        } 
                    },        
                    error: function() {
                    Swal.fire({
                        type: 'error',
                        title: '<h5>Error</h5>',
                        html: response.mensaje,
                        showCancelButton: false,
                        confirmButtonText: 'Aceptar'
                    });
                    }
                });     
            }
        });
    });
    $.ajax({
        url: 'model/',
        type: 'GET',
        dataType: 'json',
        data: {
            op: 'gestion_buscarCasoXId',
            form: {
                id:$('#id').val()
            }
        },
        success: function(response) {
            if(response.status){
                llenarFormulario(response.data);
            }else{
                mensajeResponse(response);
            } 
        },        
        error: function() {
        Swal.fire({
            type: 'error',
            title: '<h5>Error</h5>',
            html: response.mensaje,
            showCancelButton: false,
            confirmButtonText: 'Aceptar'
        });
        }
    }); 
    
    $('#btnCrear').click(function() {
        $('#frmCrearCaso').validate({
          ignore: "",
          rules: {    
           },
           errorElement: 'span',
           errorPlacement: function(error, element) {
              error.addClass('invalid-feedback');
              element.closest('.form-group').append(error);
           },
           highlight: function(element, errorClass, validClass) {
              $(element).addClass('is-invalid');
           },
           unhighlight: function(element, errorClass, validClass) {
              $(element).removeClass('is-invalid');
           },
           submitHandler: function(form) {
           form= $('#frmCrearCaso').formJson();
            form['buzon'] = form['tipologia3'];
            $.ajax({
               url: 'model/',
               type: 'POST',
               dataType: 'json',
               data: {
                    op: 'gestion_guardar',
                    form: form
               },
               success: function(response) {
                   if(response.status){
                       guardarExitoso(response.data);
                   }else{
                       guardarFallido(response);
                   } 
               },       
               error: function() {
                   Swal.fire({
                       type: 'error',
                       title: 'Error.',
                       html: "Ocurrió un error al realizar la petición",
                       showCancelButton: false,
                       confirmButtonText: 'Aceptar'
                 });
               }
             });
           }
       });
   });
 } );

 function llenarFormulario(data){
    if(data.status!='Cerrado'){
        $('#cerrar-caso').removeClass('d-none')
    }
    $('#nombreCliente').val(data.nombreCliente);
    $('#numeroOrigen').val(data.numeroOrigen)
    $('#nombreContacto').val(data.nombreContacto);
    $('#numeroContacto').val(data.numeroContacto);
    $('#status').val(data.status);
    $('#lineaNegocio').val(data.lineaNegocio);
    $('#swin').val(data.swin);
    $('#priority').val(data.priority);
    $('#fixTime').val(data.fixTime);;
    $('#tipologia1').val(data.tipologia1);
    $('#tipologia2').val(data.tipologia2);
    $('#tipologia3').val(data.tipologia3);
    $('#note').val(data.note);
    $('#resolucion').val(data.observaciones);
   

 }

 function  mensajeResponse(response,form){
    switch(response.codigo){
        case 403:        
            logout(2);      
        break;
        case 404:
            Swal.fire({
                type: 'warning',
                title: '<h5>Advertencia: </h5>',
                html: 'No se elimino el caso ',
                showCancelButton: false,
                confirmButtonText: 'Aceptar'
            });
        break;
        default:
            Swal.fire({
                type: 'warning',
                title: '<h5>Advertencia: </h5> ',
                html: response.mensaje,
                showCancelButton: false,
                confirmButtonText: 'Aceptar'
            });
        break;

    }
 }


