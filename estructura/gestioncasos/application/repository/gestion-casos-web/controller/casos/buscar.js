$(document).ready(function() {  
    $('#btnBuscarXId').click(function() {
         $('#frmBuscarXId').validate({
           ignore: "",
           rules: {
            id: {
                required: true,
                number:true
             }
           },
           messages: {
			id: {
				number: "Por favor, solo escribe numeros"
			}
		},
           errorElement: 'span',
           errorPlacement: function(error, element) {
               error.addClass('invalid-feedback');
               element.closest('.form-group').append(error);
           },
           highlight: function(element, errorClass, validClass) {
               $(element).addClass('is-invalid');
           },
           unhighlight: function(element, errorClass, validClass) {
               $(element).removeClass('is-invalid');
           },
           submitHandler: function(form) {
            form=$('#frmBuscarXId').formJson();
            $.ajax({
                url: 'model/',
                type: 'GET',
                dataType: 'json',
                data: {
                    op: 'gestion_buscarCasoXId',
                    form: form
                },
                success: function(response) {
                    if(response.status){
                        llenarFormulario(response.data);
                    }else{
                        mensajeResponse(response, form);
                    } 
                },        
                error: function() {
                Swal.fire({
                    type: 'error',
                    title: '<h5>Error</h5>',
                    html: response.mensaje,
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar'
                });
                }
            });  
            }
         });
       });
     
 
 } );

 function llenarFormulario(data){
    $('#nombreCliente').val(data.nombreCliente);
    $('#numeroOrigen').val(data.numeroOrigen);
    $('#nombreContacto').val(data.nombreContacto);
    $('#numeroContacto').val(data.numeroContacto);
    $('#status').val(data.status);
    $('#lineaNegocio').val(data.lineaNegocio);
    $('#swin').val(data.swin);    
    $('#priority').val(data.priority);
    $('#fixTime').val(data.fixTime);
    $('#tipologia1').val(data.tipologia1);
    $('#tipologia2').val(data.tipologia2);
    $('#tipologia3').val(data.tipologia3);
    $('#note').val(data.note);
    $('#resolucion').val(data.observaciones);
    $('#fixTime').val(data.fixTime);
    $('#info-caso').removeClass('d-none');
 }

 function  mensajeResponse(response,form){
    switch(response.codigo){
        case 403:        
            logout(2);      
        break;
        case 404:
            
            Swal.fire({
                type: 'warning',
                title: '<h5>Advertencia: </h5>',
                html: 'No se encontro el caso '+form['id'],
                showCancelButton: false,
                confirmButtonText: 'Aceptar'
            });
        break;
        default:
            Swal.fire({
                type: 'warning',
                title: '<h5>Advertencia: </h5> ',
                html: response.mensaje,
                showCancelButton: false,
                confirmButtonText: 'Aceptar'
            });
        break;

    }
 }


