
/**************************************************************************************************************************** */
$(document).ready(function() {   
    $('#btnCrear').click(function() {
        $('#frmCrearCaso').validate({
          ignore: "",
          rules: {
            tipologia1: {
                required: true
             },
             tipologia2: {
                required: true
             },
             tipologia3: {
                required: true
             },
             fixTime: {
                required: true
             },
             nombreContacto: {
                required: true,
                lettersonly: true
             },
             numeroContacto: {
                required: true,
                number: true,
                minlength: 8,
                maxlength: 8
             },
             numeroOrigen: {
                number: true,
                minlength: 8,
                maxlength: 8
             }
           },
           messages: {
			tipologia1: {
				required: "Por favor, elige una tipolog&iacute;a"
			},
            tipologia2: {
				required: "Por favor, elige una tipolog&iacute;a"
			},
            tipologia3: {
				required: "Por favor, elige una tipolog&iacute;a"
			},
            nombreContacto: {
                lettersonly: "Por favor, escribe solo letras"

             },
             numeroContacto: {
                required: "N&uacute;mero contacto obligatorio",
                number: "Por favor, solo n&uacute;meros",
                minlength: "Por favor, ingresa 8 numeros",
                maxlength: "Por favor, ingresa 8 numeros"
             }
		},
           errorElement: 'span',
           errorPlacement: function(error, element) {
              error.addClass('invalid-feedback');
              element.closest('.form-group').append(error);
           },
           highlight: function(element, errorClass, validClass) {
              $(element).addClass('is-invalid');
           },
           unhighlight: function(element, errorClass, validClass) {
              $(element).removeClass('is-invalid');
           },
           submitHandler: function(form) {
           form= $('#frmCrearCaso').formJson();
            form['buzon'] = form['tipologia3'];
            
            $.ajax({
               url: 'model/',
               type: 'POST',
               dataType: 'json',
               data: {
                    op: 'gestion_guardar',
                    form: form
               },
               success: function(response) {
                   if(response.status){
                       guardarExitoso(response.data);
                   }else{
                       guardarFallido(response);
                   } 
               },       
               error: function() {
                   Swal.fire({
                       type: 'error',
                       title: 'Error.',
                       html: "Ocurrió un error al realizar la petición",
                       showCancelButton: false,
                       confirmButtonText: 'Aceptar'
                 });
               }
             });
           }
       });
   });
   
  
    $('#numeroOrigen').blur(function() {
       if ($('#numeroOrigen').val() != '') {
            obtenerLineaNegocio($('#numeroOrigen').val(), 1);
        }
    });
    $('#swin').blur(function() { 
        if ($('#swin').val() != '') {
            obtenerLineaNegocio($('#swin').val(), 2);
        } 
    });
    $('#fixTime').change(function () {
        obtenerTipologia1();
    });
    $('#lineaNegocio').change(function() {
        obtenerTipologia1();
    });

    $('#tipologia1').change(function() {
        obtenerTipologia2();
    });
    $('#tipologia2').change(function() {
        obtenerTipologia3();
    });
    $('#tipologia1').select2({
        theme:'bootstrap4',
        placeholder: 'Selecciona una opción',
        allowClear: true, 
        minimumInputLength:0,
        minimumResultForSearch:0,
        width: '100%',
        dropdownAutoWidth: true,
        language: {
            noResults: function() {
              return "No se encontraron resultados.";
            }
          },
    });
    $('#tipologia2').select2({
        theme:'bootstrap4',
        placeholder: 'Selecciona una opción',
        allowClear: true, 
        minimumInputLength:0,
        minimumResultForSearch:0,
        width: '100%',
        dropdownAutoWidth: true,
        language: {
            noResults: function() {
              return "No se encontraron resultados.";
            }
          },
    });
    $('#tipologia3').select2({
        theme:'bootstrap4',
        placeholder: 'Selecciona una opción',
        allowClear: true, 
        minimumInputLength:0,
        minimumResultForSearch:0,
        width: '100%',
        dropdownAutoWidth: true,
        language: {
            noResults: function() {
              return "No se encontraron resultados.";
            }
          },
    });
});


function obtenerLineaNegocio(parametro, tipo) {
    $('#lineaNegocio').empty();
    if (parametro != '') {
        $.ajax({
            url: 'model/',
            type: 'GET',
            dataType: 'json',
            data: {
                op: 'gestion_lineaNegocio',
                parametro: parametro
            },
            success: function (response) {
                if (response.status) {
                    $('#lineaNegocio').val(response.data.lineaNegocio);
                    if (tipo == 1) {
                        $('#swin').val('');
                        $('#col-swin').addClass('d-none');
                        $('#col-numOrigen').removeClass('col-sm-6').addClass('col-12');

                    } else {
                        $('#numeroOrigen').val('');
                        $('#col-numOrigen').addClass('d-none');
                        $('#col-swin').removeClass('col-sm-6').addClass('col-12');
                    }
                    obtenerTipologia1();
                } else {
                    if (response.codigo == 403) {
                        logout(2);   
                    }
                    if (tipo == 1) {
                        $('#col-swin').removeClass('d-none');
                        $('#col-numOrigen').removeClass('col-12').addClass('col-sm-6');

                    } else {
                        $('#col-numOrigen').removeClass('d-none');
                        $('#col-swin').removeClass('col-12').addClass('col-sm-6');
                    }
                    $('#lineaNegocio').val('');
                    $('#tipologia1').html('<option value="">Seleccione la tipologia 1</option>');
                    $('#tipologia2').html('<option value="">Seleccione la tipologia 2</option>');
                    $('#tipologia3').html('<option value="">Seleccione la tipologia 3</option>');
                }
            },
            error: function () {
                Swal.fire({
                    type: 'error',
                    title: 'Error.',
                    html: "Ocurrió un error al realizar la petición",
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar'
                });
            }
        });
    } else {
        $('#lineaNegocio').val('');
        $('#tipologia1').html('<option value="" >Seleccione la tipologia 1</option>');
        $('#tipologia2').html('<option value="" >Seleccione la tipologia 2</option>');
        $('#tipologia3').html('<option value="" >Seleccione la tipologia 3</option>');
        if (tipo == 1) {
            $('#swin').removeAttr("readonly");
        } else {
            $('#numeroOrigen').removeAttr("readonly");
        }

    }
}
function obtenerTipologia1() {
    if ($('#lineaNegocio').val() != null && $('#lineaNegocio').val() != '' &&
        $('#fixTime').val() != null && $('#fixTime').val() != '') {

        $.ajax({
            url: 'model/',
            type: 'POST',
            dataType: 'json',
            data: {
                op: 'gestion_obtenerTipologia1',
                form: {
                    lineaNegocio: $('#lineaNegocio').val(),
                    areaOrigen: $('#fixTime').val()
                },
            },
            success: function (response) {
                if (response.status) {
                    $('#tipologia1 #tipologia2 #tipologia3').empty();
                    $('#tipologia1').html('<option value="">Seleccione la tipologia 1</option>');
                    $('#tipologia2').html('<option value="">Seleccione la tipologia 2</option>');
                    $('#tipologia3').html('<option value="">Seleccione la tipologia 3</option>');
                    $.each(response.data, function (index, value) {
                        $('#tipologia1').append(
                            '<option value="' + value + '">' +
                            value +
                            '</option>'
                        );
                    });
                } else {
                    if (response.codigo == 403) {
                        logout(2);
                    }
                }
            },
            error: function () {
                Swal.fire({
                    type: 'error',
                    title: 'Error.',
                    html: "Ocurrió un error al realizar la petición",
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar'
                });
            }
        });
    }
}

function obtenerTipologia2() {
    $.ajax(
        { 
            url: 'model/',
            type: 'POST',
            dataType: 'json',
            data: {
                op: 'gestion_obtenerTipologia2',
                form: {
                    lineaNegocio: $('#lineaNegocio').val(),
                    areaOrigen: $('#fixTime').val(),
                    tpg1: $('#tipologia1').val()
                },
        },
        success: function (response) {
            if (response.status) {
                $('#tipologia2 #tipologia3').empty();
                $('#tipologia2').html('<option value="">Seleccione la tipologia 2</option>');
                $('#tipologia3').html('<option value="">Seleccione la tipologia 3</option>');
                $.each(response.data, function (index, value) {
                    $('#tipologia2').append(
                        '<option value="' + value + '">' +
                        value +
                        '</option>'
                    );
                });
            }
        },
        error: function () {
            Swal.fire({
                type: 'error',
                title: 'Error.',
                html: "Ocurrió un error al realizar la petición",
                showCancelButton: false,
                confirmButtonText: 'Aceptar'
            });
        }
    });

}

function obtenerTipologia3() {
    $.ajax(
        { 
            url: 'model/',
            type: 'POST',
            dataType: 'json',
            data: {
                op: 'gestion_obtenerTipologia3',
                form: {
                    lineaNegocio: $('#lineaNegocio').val(),
                    areaOrigen: $('#fixTime').val(),
                    tpg1: $('#tipologia1').val(),
                    tpg2: $('#tipologia2').val()
                },
        },
        success: function (response) {
            if (response.status) {
                $('#tipologia3').empty();
                $('#tipologia3').html('<option value="">Seleccione la tipologia 3</option>');
                $.each(response.data, function (index, value) {
                    $('#tipologia3').append(
                        '<option value="' + value + '">' +
                        value +
                        '</option>'
                    );
                });
            }
        },
        error: function () {
            Swal.fire({
                type: 'error',
                title: 'Error.',
                html: "Ocurrió un error al realizar la petición",
                showCancelButton: false,
                confirmButtonText: 'Aceptar'
            });
        }
    });

}



function guardarExitoso(data){
    Swal.fire({
        type: 'info',
        title: '<h5> Id: '+data.id+'</h5>' ,
        html: 'Se creo el caso exitosamente',
        showCancelButton: false,
        confirmButtonText: 'Aceptar'
    }).then((result) => {
        location.href='detalle/'+data.id;
      });
}

function guardarFallido(response){
    switch(response.codigo){
        case 403:        
            logout(2);      
        break;
        case 404:
            Swal.fire({
                type: 'warning',
                title: '<h5>Advertencia: </h5>',
                html: 'No se guardo el caso ',
                showCancelButton: false,
                confirmButtonText: 'Aceptar'
            });
        break;
        default:
            Swal.fire({
                type: 'warning',
                title: '<h5>Advertencia: </h5> ',
                html: response.mensaje,
                showCancelButton: false,
                confirmButtonText: 'Aceptar'
            });
        break;

    }
}

