$(document).ready(function () {
    $('#btnBuscar').click(function () {
            $('#frmBuscarXTelefono').validate({
                ignore: "",
                rules: {
                    numeroContacto: {
                        required: true
                    }
                },
                messages: {
                    numeroContacto: {
                        required: "Campo es obligatorio",

                    }
                },
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                },
                submitHandler: function (form) {
                    form = $('#frmBuscarXTelefono').formJson();
                    obtenerCasos(form);

                }
            });
    });
});

function obtenerCasos(form) {
    $.ajax({
        url: 'model/',
        type: 'GET',
        dataType: 'json',
        data: {
            op: 'gestion_buscarXTelefono',
            form: form
        },
        success: function (response) {
            $('#info-vacio').addClass('d-none');
            $('#info').addClass('d-none');
            if ($.fn.dataTable.isDataTable('#tblCasos')) {
                $('#tblCasos').DataTable().destroy();
                $('#tblCasos tbody').empty();
            }
            if (response.status) {
                llenarDataTable(response.data);
            } else {
                mensajeResponse(response, form);
            }
        },
        error: function () {
            Swal.fire({
                type: 'error',
                title: '<h5>Error</h5>',
                html: response.mensaje,
                showCancelButton: false,
                confirmButtonText: 'Aceptar'
            });
        }
    });
}

function llenarDataTable(data) {
    $('#info-vacio').addClass('d-none');
    $('#tblBodyCasos').empty();
    $.each(data, function (index, value) {
        $('#tblBodyCasos').append(
            '<tr>' +
            '<td><a href="detalle/' + value.id + '" class="btn btn-primary" >' + value.id + '</a></td>' +
            '<td><span>' + ((value.tipologia3 != null && value.tipologia3 != '') ? value.tipologia3 : '') + '</span></td>' +
            '<td>' + ((value.numeroOrigen != null && value.numeroOrigen != '') ? value.numeroOrigen : ((value.swin != null && value.swin != '') ? value.swin : '')) + '</td>' +
            '<td>' + ((value.status != null && value.status != '') ? value.status : '') + '</td>' +
            '</tr>'
        );
    });
    $('#tblCasos').dataTable({
        destroy: true,
        order: [
            [0, 'desc']
        ],
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });

    $('#info').removeClass('d-none');
}

function mensajeResponse(response, form) {
    switch (response.codigo) {
        case 403:
            logout(2);
            break;
        case 404:
            $('#info-vacio').removeClass('d-none');
            break;
        default:
            Swal.fire({
                type: 'warning',
                title: '<h5>Advertencia: </h5> ',
                html: response.codigo,
                showCancelButton: false,
                confirmButtonText: 'Aceptar'
            });
            break;

    }
}