<?php
	require_once('config/settings.inc.php');

	$page = array('file'=>'', 'template'=>'');

	
/*	$page['error'] = array(
		'file' => 'system/404.php',
		'template' => __TEMPLATE_ERROR__
	);*/
	
	if (isset($_SESSION[__SESSION_NAME__])) {
		
		$page['crear'] = array(
			'file' => 'casos/crear.php',
			'template' => __TEMPLATE_DESKTOP__
		);
		$page['buscar'] = array(
			'file' => 'casos/buscar.php',
			'template' => __TEMPLATE_DESKTOP__
		);
		$page['detalle'] = array(
			'file' => 'casos/detalle.php',
			'template' => __TEMPLATE_DESKTOP__
		);
		$page['buscar-telefono'] = array(
			'file' => 'casos/buscar-telefono.php',
			'template' => __TEMPLATE_DESKTOP__
		);
		$page['login'] = array(
			'file' => 'casos/crear.php',
			'template' => __TEMPLATE_DESKTOP__
		);	
		$page['error'] = array(
			'file' => 'casos/crear.php',
			'template' => __TEMPLATE_DESKTOP__
		);
		
	} else {
		$page['login'] = array(
			'file' => 'system/login.php',
			'template' => __TEMPLATE_LOGIN__
		);	
		$page['error'] = array(
			'file' => 'system/login.php',
			'template' => __TEMPLATE_LOGIN__
		);			
	}
?>
