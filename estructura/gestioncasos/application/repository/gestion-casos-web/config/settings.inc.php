<?php
	# DEFINIR ENCABEZADOS
	# Indicarle a PHP que se ha de iniciar el buffering de la salida
	@ob_start();
	# Crea una sesión o reanuda la actual basada en un identificador de sesión
	@session_start();
	# Establece el uso horario que utilizan las funciones de fecha y hora
	date_default_timezone_set('America/El_Salvador'); 
	# Indica la configuración regional
	setlocale(LC_TIME, 'spanish');
	# Capa de seguridad adicional que bloquea ataques XSS
	header('X-XSS-Protection: 1; mode=block');
	# Codificación de caracteres
	header('Content-Type: text/html; charset=utf-8');
	# Limpiar cache del navegador
	header('Cache-Control: no-cache, must-revalidate');
	header('Expires: Mon, 20 Dec 2000 01:00:00 GMT');

	# Configuración de sistema desarrollo o producción
	# dev, prod || Configuración errores
	# E_ERROR, E_ALL || Configuración errores
	define('__MODE__', 'dev');
	if (__MODE__=='prod') {
		error_reporting(E_ERROR);
	} else {
		error_reporting(E_ALL);
		ini_set('display_errors', '1');
		ini_set('display_startup_errors', '1');
	}

	# Configuración de datos del sistema
	define('__SYSTEM_NAME__', 'Web Gestion de Casos');
	define('__SYSTEM_ABREV__', 'Gestion Casos');
	define('__SYSTEM_VERSION__', '1.0.0');

	# Nombre de variable de session 
	define('__SESSION_NAME__', 'gestion-casos-web');

	# Definir ruta del proyecto (nombre de carpeta del proyecto)
	define('__BASE_URI__', '/');
	# http, https || Configuración HTTP 
	define('__HTTP__', 'http');
	define('__HTTPS__', 'https');
	# localhost,  || Configuración de dominio y nombre de carpeta 
	define('__DOMAIN__', 'atencionb2b.movistar.com.sv');

	# Configuración de consumo de API
	# Definir directorio del api
	define('__BASE_URI_API__', '/wsGCasos/api/');
	# Puerto a utilizar
	define('__API_PORT__', '8080');
	# localhost,  || Configuración de dominio y nombre de carpeta 
	define('__DOMAIN_API__', 'gestion-casos');
	# Ruta del API
	define('__BASE_URI_HTTP_API__', __HTTP__.'://'.__DOMAIN_API__.':'.__API_PORT__.__BASE_URI_API__);

	# Configuración de rutas para librerias, modelos y paginas del sitio
	define('__BASE_URI_HTTP__', __HTTPS__.'://'.__DOMAIN__.__BASE_URI__);
	define('__BASE_URI_SYS__', '/var/www/html/gestion-casos-web'.__BASE_URI__);
	# define('__BASE_URI_SYS__', 'http://'.__DOMAIN__.__BASE_URI__);
	# define('__BASE_URI_SYS__', '/Library/WebServer/Documents'.__BASE_URI__);
	
	#define('__BASE_URI_SYS__', 'C:/xampp/htdocs'.__BASE_URI__);

	# CONFIGURACIÓN PARA ARCHIVO config.inc.php
	# Define el directorio de las vistas y plantillas
	define('__VIEW_PATH__', realpath('view'));
	define('__TEMPLATE_PATH__', realpath('template'));
	# Define las plantillas a utilizar en el proyecto
	define('__TEMPLATE_LOGIN__', 'login.php');
	define('__TEMPLATE_DESKTOP__', 'desktop.php');
	define('__TEMPLATE_ERROR__', 'error.php');
	# Define la vistas a por defectos (estas estan definidas en el archivo config.inc.php)
	define('__VIEW_LOGIN__', 'login');
	define('__VIEW_ERROR__', 'error');
	define('__VIEW_MAINPAGE__', 'crear');

	
	
?>
