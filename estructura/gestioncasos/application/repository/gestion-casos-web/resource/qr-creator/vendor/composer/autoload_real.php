<?php

// autoload_real.php @generated by Composer

class ComposerAutoloaderInit2b8341ff0cd7e2b9f72b3bd2a84e894e
{
    private static $loader;

    public static function loadClassLoader($class)
    {
        if ('Composer\Autoload\ClassLoader' === $class) {
            require __DIR__ . '/ClassLoader.php';
        }
    }

    /**
     * @return \Composer\Autoload\ClassLoader
     */
    public static function getLoader()
    {
        if (null !== self::$loader) {
            return self::$loader;
        }

        require __DIR__ . '/platform_check.php';

        spl_autoload_register(array('ComposerAutoloaderInit2b8341ff0cd7e2b9f72b3bd2a84e894e', 'loadClassLoader'), true, true);
        self::$loader = $loader = new \Composer\Autoload\ClassLoader(\dirname(__DIR__));
        spl_autoload_unregister(array('ComposerAutoloaderInit2b8341ff0cd7e2b9f72b3bd2a84e894e', 'loadClassLoader'));

        $includePaths = require __DIR__ . '/include_paths.php';
        $includePaths[] = get_include_path();
        set_include_path(implode(PATH_SEPARATOR, $includePaths));

        require __DIR__ . '/autoload_static.php';
        \Composer\Autoload\ComposerStaticInit2b8341ff0cd7e2b9f72b3bd2a84e894e::getInitializer($loader)();

        $loader->register(true);

        return $loader;
    }
}
