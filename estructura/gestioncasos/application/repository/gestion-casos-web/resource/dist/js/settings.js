var isMobile = {
    Device: function() {    
        let device = new Array;
        device.mobile = false;
        device.type = 'This is a tablet or desktop';
        if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/iPhone|iPad|iPod/i) || navigator.userAgent.match(/Opera Mini/i) || navigator.userAgent.match(/IEMobile/i) || window.matchMedia('only screen and (max-width: 767px)').matches){
            device.mobile = true;
            device.type = navigator.userAgent;
        }
        return device;
    }
};

$(function () {


    // Limpia consola del navegador y advierte al usuario de no ingresar codigo
    function clearConsole(){
        if(window.console || window.console.firebug) {
            //console.clear();
            console.log('%c\n¡Detente!\n', "font-family: Arial; font-size: 35pt; color: red; font-weight: bold; -webkit-text-fill-color: red; -webkit-text-stroke-color: black; -webkit-text-stroke-width: 0.75px;");
            console.log('%cEsta función del navegador está pensada para desarrolladores. Si alguien te indicó que copiaras y pegaras algo aquí para habilitar una función del sistema o para "hackear" los datos de alguien, se trata de un fraude.\n', "font-family: Arial; font-size: 14pt; color: #444; margin-top: 15px;");
            console.log('%c\nMovistar\n', "font-family: Arial; font-size: 20pt; color: orange; font-weight: bold; -webkit-text-fill-color: orange; -webkit-text-stroke-color: black; -webkit-text-stroke-width: 0.75px;");
        }
    }

    if(typeof clearConsole === 'function') {
      //  clearConsole();
    }

    // console.log(isMobile.Device());
});
// Funcion que convierte formulario a Json
$.fn.formJson = function(){
    var formJson = {};
    var formSerial = this.serializeArray();
    $.each(formSerial, function () {
        if (formJson[this.name]) {
            if (!formJson[this.name].push) {
                formJson[this.name] = [formJson[this.name]];
            }
            formJson[this.name].push(this.value || '');
        } else {
            formJson[this.name] = this.value || '';
        }
    });
    var formCheckbox = $('input[type=checkbox]',this);
    $.each(formCheckbox,function(){
        if(!formJson.hasOwnProperty(this.name)){
            formJson[this.name] = 0;
        }
    });
    return formJson;
};

// Funcion que permite limpiar un formulario
$.fn.clearForm = function() {
    var form=this[0];
    $(':input', form).each(function() {
        var type = this.type;
        var tag = this.tagName.toLowerCase();
        var $this = $(this);
        if (type == 'text' || type == 'hidden' || type == 'password' || type == 'number' || type == 'date' || type == 'email' || tag == 'textarea'){
            this.value = "";
        }else if (type == 'checkbox'){
            $(this).removeAttr('checked');
        }else if (tag == 'select' && this.multiple){
            this.selectedIndex = -1;
        }else if (tag == 'select') {
            this.selectedIndex = -1;
        }
    });
    form.reset();
};

// Funcion que permite cargar un formulario con los datos de nuestra base de datos
$.fn.formFieldValues = function(data) {
    var els = this.find(':input').get();
    if(arguments.length === 0) {
        data = {};
        $.each(els, function() {
            if (this.name && !this.disabled && (this.checked || /select|textarea/i.test(this.nodeName) || /text|hidden|password|number|date|email/i.test(this.type))){
                if(data[this.name] == undefined){
                    data[this.name] = [];
                }
                data[this.name].push($(this).val());
            }
        });
        return data;
    } else {
        $.each(els, function() {
            if (this.name && data[this.name]){
                var names = data[this.name];
                var $this = $(this);
                if(Object.prototype.toString.call(names) !== '[object Array]'){
                    names = [names];
                }
                if(this.type == 'checkbox' || this.type == 'radio'){
                    var val = $this.val();
                    var found = false;
                    for(var i = 0; i < names.length; i++){
                        if(names[i] == val){
                            found = true;
                            break;
                        }
                    }
                    $this.attr("checked", found);
                } else {
                    $this.val(names[0]);
                }
            }
        });
        return this;
    }
};